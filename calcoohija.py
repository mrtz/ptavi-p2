#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):
    # el init ya no hace falta porque lo coge de la calculadora madre
    def mult(self):
        return self.op1 * self.op2

    def div(self):
        try:
            return self.op1 / self.op2
        except ZeroDivisionError:
            sys.exit('Division by zero is not allowed')


if __name__ == "__main__":  # programa principal
    try:
        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
        calc = CalculadoraHija(op1, op2)
    except ValueError:
        sys.exit('Error: no numerical parameters')

    if sys.argv[2] == "suma":
        result = calc.sum()

    elif sys.argv[2] == "resta":
        result = calc.minus()

    elif sys.argv[2] == 'multiplicacion':
        result = calc.mult()

    elif sys.argv[2] == 'division':
        result = calc.div()

    else:
        sys.exit('Solo acepta: suma, resta, multiplicacion y division')
    print(result)
