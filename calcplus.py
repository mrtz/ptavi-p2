#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija

if __name__ == "__main__":

    fichero = open(sys.argv[1], "r")
    lineas = fichero.readlines()
    fichero.close()

    for linea in lineas:
        posicion = linea.split(',')  # la ',' delimita cada posicion
        result = int(posicion[1])  # sum parcl se conv en 1er op de la sum final
        operacion = posicion[0]

        if operacion == 'suma':
            for elemento in posicion[2:]:  # pa los elmnts a partir del 2º (3º)
                result = calcoohija.CalculadoraHija(result, int(elemento)).sum()  # los 2 primeros numeros se suman con el siguiente, así en bucle
            print(result)

        if operacion == 'resta':
            for elemento in posicion[2:]:
                result = calcoohija.CalculadoraHija(result, int(elemento)).minus()
            print(result)

        if operacion == 'multiplica':
            for elemento in posicion[2:]:
                result = calcoohija.CalculadoraHija(result, int(elemento)).mult()
            print(result)

        if operacion == 'divide':
            for elemento in posicion[2:]:
                result = calcoohija.CalculadoraHija(result, int(elemento)).div()
            print(result)
