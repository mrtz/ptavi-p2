# !/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import calcoohija
import csv


if __name__ == '__main__':
    with open(sys.argv[1]) as file:
        lineas = csv.reader(file, delimiter=',')  # no es necesario poner el delimiter porque tenemos los elementos seprados por '','
        for posicion in lineas:
            result = int(posicion[1])
            operacion = posicion[0]

            if operacion == 'suma':
                for elemento in posicion[2:]:
                    result = calcoohija.CalculadoraHija(result, int(elemento)).sum()
                print(result)

            if operacion == 'resta':
                for elemento in posicion[2:]:
                    result = calcoohija.CalculadoraHija(result, int(elemento)).minus()
                print(result)

            if operacion == 'multiplica':
                for elemento in posicion[2:]:
                    result = calcoohija.CalculadoraHija(result, int(elemento)).mult()
                print(result)

            if operacion == 'divide':
                for elemento in posicion[2:]:
                    result = calcoohija.CalculadoraHija(result, int(elemento)).div()
                print(result)
