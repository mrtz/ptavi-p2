
# !/usr/bin/python3
# *- coding: utf-8 -*-


import sys


class Calculadora():
    def __init__(self, op1, op2):  # en self se guarda la refer a mi objeto
        self.op1 = op1
        self.op2 = op2

    def sum(self):
        return self.op1 + self.op2

    def minus(self):
        return self.op1 - self.op2


if __name__ == "__main__":
    try:

        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
        calc = Calculadora(op1, op2)
    except ValueError:
        sys.exit('Error: no numerical parameters')

    if sys.argv[2] == "suma":  # sys.argv[2] es la operacion
        result = calc.sum()

    elif sys.argv[2] == "resta":
        result = calc.minus()
    else:
        sys.exit('La peración sólo puede ser una suma o una resta.')
    print(result)
